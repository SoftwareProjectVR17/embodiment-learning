﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSVOutputGrid
{
	public string[,] grid;
	public int linesLength;

	public CSVOutputGrid(string[,] grid, int linesLength)
	{
		this.grid = grid;
		this.linesLength = linesLength;
	}
}
