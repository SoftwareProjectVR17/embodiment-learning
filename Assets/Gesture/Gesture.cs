﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Gesture
{
	public enum GestureType {Normal, Mean, Variance};

	static string directoryPath = "Assets/Record/Recorded/";
	public List<GestureState> states;
	string name;

	public Gesture()
	{
		states = new List<GestureState> ();
	}

	public Gesture (string name, GestureType type = GestureType.Normal, Gesture gestureMean = null)
	{
		states = new List<GestureState> ();
		this.name = name;
		if (type == GestureType.Mean)
			LoadMean ();
		else if (type == GestureType.Variance)
			LoadVariance (gestureMean);
	}

	void LoadMean ()
	{
		string path = "Assets/Record/Recorded/" + name + "/mean.txt";
		if(File.Exists(path))
			Load (path);
		else
		{
			ComputeMean ();
			Save (path);
		}
	}

	void ComputeMean ()
	{
		//Debug.Log ("[Gesture:ComputeMean] Start computing mean ");

		//find and load gestures to compare for the mean
		List<Gesture> gesturesToMean = new List<Gesture> ();
		string path = "Assets/Record/Recorded/" + name + "/";
		//Debug.Log ("[Gesture:ComputeMean] path " + path);
		foreach (string gesturePath in Directory.GetFiles (path))
		{
			//Debug.Log ("[Gesture:ComputeMean] gesturePath " + gesturePath);
			if (gesturePath.Substring(gesturePath.Length - 4).Equals(".txt") &&
				!gesturePath.Equals (path + "mean.txt") && !gesturePath.Equals (path + "variance.txt"))
			{
				Gesture gesture = new Gesture(name);
				gesture.Load (gesturePath);
				gesturesToMean.Add(gesture);
			}
		}

		if (gesturesToMean.Count == 0)
			return;
		

		//find mean time and normalize towards it
		int meanTime = 0;
		foreach (Gesture gesture in gesturesToMean)
			meanTime += gesture.states.Count;
		meanTime /= gesturesToMean.Count;


		//for each frame, compute position and rotation mean
		for (int i = 0; i < meanTime; i++)
		{
			Vector3 positionMean = new Vector3 (0, 0, 0);
			Vector4 rotationMean = new Vector4 (0, 0, 0, 0);
			float timestampMean = 0.0f;

			int gestureCount = 0;
			foreach (Gesture gesture in gesturesToMean)
			{
				if (gesture.states.Count > i)
				{
					timestampMean += gesture.states [i].timestamp;
					positionMean += gesture.states [i].position;
					rotationMean += gesture.states [i].rotation;
					gestureCount ++;
				}
			}

			timestampMean /= gestureCount;
			positionMean /= gestureCount;
			rotationMean /= gestureCount;

			GestureState stateMean = new GestureState ();
			stateMean.position = positionMean;
			stateMean.rotation = rotationMean;
			stateMean.timestamp = timestampMean;
			states.Add (stateMean);
		}

		Debug.Log ("[Gesture:ComputeMean] Finished computing mean " + meanTime);
	}

	void LoadVariance (Gesture gestureMean = null)
	{
		string path = "Assets/Record/Recorded/" + name + "/variance.txt";
		if (File.Exists (path))
			Load (path);
		else
		{
			ComputeVariance (gestureMean);
			Save (path);
		}
	}

	void ComputeVariance (Gesture gestureMean)
	{
		//find and load gestures to compare for the variance
		List<Gesture> gesturesToVariance = new List<Gesture> ();
		string path = "Assets/Record/Recorded/" + name + "/";

		foreach (string gesturePath in Directory.GetFiles (path))
		{
			if (gesturePath.Substring(gesturePath.Length - 4).Equals(".txt") &&
				!gesturePath.Equals (path + "mean.txt") && !gesturePath.Equals (path + "variance.txt"))
			{
				Gesture gesture = new Gesture(name);
				gesture.Load (gesturePath);
				gesturesToVariance.Add(gesture);
			}
		}

		if (gesturesToVariance.Count == 0)
			return;


		//calculate variance
		for(int i = 0; i < gestureMean.states.Count; i++)
		{
			Vector3 positionVariance = new Vector3 (0, 0, 0);
			Vector4 rotationVariance = new Vector4 (0, 0, 0, 0);
			float timestampVariance = 0.0f;

			int gestureCount = 0;
			foreach (Gesture gesture in gesturesToVariance)
			{
				if (gesture.states.Count > i)
				{
					timestampVariance += (gesture.states [i].timestamp - gestureMean.states [i].timestamp) * 
						(gesture.states [i].timestamp - gestureMean.states [i].timestamp);

					positionVariance.x += Mathf.Abs(gesture.states [i].position.x - gestureMean.states [i].position.x);
					positionVariance.y += Mathf.Abs(gesture.states [i].position.y - gestureMean.states [i].position.y);
					positionVariance.z += Mathf.Abs(gesture.states [i].position.z - gestureMean.states [i].position.z);

					rotationVariance.x += Mathf.Abs(gesture.states [i].rotation.x - gestureMean.states [i].rotation.x);
					rotationVariance.y += Mathf.Abs(gesture.states [i].rotation.y - gestureMean.states [i].rotation.y);
					rotationVariance.z += Mathf.Abs(gesture.states [i].rotation.z - gestureMean.states [i].rotation.z);
					rotationVariance.w += Mathf.Abs(gesture.states [i].rotation.w - gestureMean.states [i].rotation.w);
					
					gestureCount ++;
				}
			}

			timestampVariance /= gestureCount;
			positionVariance /= gestureCount;
			rotationVariance /= gestureCount;

			GestureState stateMean = new GestureState ();
			stateMean.position = positionVariance;
			stateMean.rotation = rotationVariance;
			stateMean.timestamp = timestampVariance;
			states.Add (stateMean);
		}
	}

	public void Load (string path)
	{
		states = GestureLoader.Load (path);
	}

	void Save(string path)
	{
		StreamWriter mStreamWriter;
		mStreamWriter = new StreamWriter(path);
		mStreamWriter.WriteLine("Gesture: " + name + "; Author: none");

		foreach(GestureState state in states)
		{
			string line = string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}",
				state.timestamp, 
				state.position.x, state.position.y, state.position.z, 
				state.rotation.x, state.rotation.y, state.rotation.z, state.rotation.w, 
				0, 0, 0,
				0, 0, 0, 0);
			
			mStreamWriter.WriteLine(line);
		}

		mStreamWriter.Close();
	}

	public override string ToString ()
	{
		return name;
	}
}
