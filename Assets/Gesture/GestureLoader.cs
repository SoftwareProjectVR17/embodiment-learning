﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GestureLoader
{
	public static List<GestureState> Load (string fullpath)
	{ 
		StreamReader reader = new StreamReader(fullpath); 
		CSVOutputGrid csvGrid = CSVReader.SplitCsvGrid (reader.ReadToEnd ());
		string[,] csvStrings = csvGrid.grid;

		bool loadLeftHand = true;
		bool handDetermined = false;

		List<GestureState> states = new List<GestureState>();
		for (int i = 0; i < csvGrid.linesLength; i++)
		{
			if (i < 2) //dont take 2 first lines into account
				continue;

			if (csvStrings == null)
				break;

			//we need timestamp, x, y, z of palm
			string[] line = csvStrings[0,i].Split (' ');

			//determine which hand to load
			if (!handDetermined && float.Parse (line [1]) == 0)
			{
				loadLeftHand = false;
				handDetermined = true;
			}

			//for now we only save the left palm's coordinates
			GestureState state = new GestureState ();
			state.timestamp = float.Parse(line [0]);
			if (loadLeftHand)
			{
				state.position.x = float.Parse (line [1]);
				state.position.y = float.Parse (line [2]);
				state.position.z = float.Parse (line [3]);
				state.rotation.x = float.Parse (line [4]);
				state.rotation.y = float.Parse (line [5]);
				state.rotation.z = float.Parse (line [6]);
				state.rotation.w = float.Parse (line [7]);
			}
			else
			{
				state.position.x = float.Parse (line [8]);
				state.position.y = float.Parse (line [9]);
				state.position.z = float.Parse (line [10]);
				state.rotation.x = float.Parse (line [11]);
				state.rotation.y = float.Parse (line [12]);
				state.rotation.z = float.Parse (line [13]);
				state.rotation.w = float.Parse (line [14]);
			}
			states.Add (state);
		}
		reader.Close();

		return states;
	}
}
