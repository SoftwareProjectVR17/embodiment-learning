﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureState {
	
	public float timestamp = 0;
	public Vector3 position = new Vector3(0, 0, 0);
	public Vector4 rotation = new Vector4(0, 0, 0, 0);

	public override string ToString()
	{
		return "State timestamp " + timestamp + ", x: " + position.x + ", y: " + position.y + ", z: " + position.z +
			", rx: " + rotation.x + ", ry: " + rotation.y + ", rz: " + rotation.z + ", rw: " + rotation.w;
	}
}
