﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToReplayButton : MonoBehaviour {

	public void GoToReplayScene()
	{
		SceneManager.LoadScene ("Replay");
	}
}
