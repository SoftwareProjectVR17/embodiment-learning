using Leap;
using Leap.Unity;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Recorder : MonoBehaviour
{
	public GameObject recordingLabel = null;
	public GameObject recordingNumberLabel = null;
	public GameObject startButton = null;

	public LeapServiceProvider LeapService;
	public string gestureName = "gesture";
	public string userName = "user";
	public int recordingNumber = 0;

	private string path = "Assets/Record/Recorded/";
	private StreamWriter mStreamWriter;
	private string date;


	public void ToggleRecording()
	{
		if (mStreamWriter == null)
			StartRecording ();
		else
			StopRecording ();
	}

	private void StartRecording()
	{
		Directory.CreateDirectory (path + gestureName);
		mStreamWriter = new StreamWriter(path + gestureName + "/" + recordingNumber.ToString() + ".txt");
		mStreamWriter.WriteLine("Gesture: " + gestureName + "; Author: " + userName + "; Recorded: " + date);
		mStreamWriter.WriteLine("timestamp l_palm_pos_x l_palm_pos_y l_palm_pos_z r_palm_pos_x r_palm_pos_y r_palm_pos_z");

		//UI
		recordingLabel.SetActive (true);
		startButton.GetComponentInChildren<Text> ().text = "Stop";
	}

	private void StopRecording()
	{
		mStreamWriter.Close();
		mStreamWriter = null;

		//UI
		recordingLabel.SetActive (false);

		recordingNumber++;
		recordingNumberLabel.GetComponentInChildren<InputField> ().text = recordingNumber.ToString ();
		startButton.GetComponentInChildren<Text> ().text = "Start";
	}

	void Update ()
	{
		if (mStreamWriter != null)
		{
			Vector3 leftPalmPosition = Vector3.zero, rightPalmPosition = Vector3.zero;
			Vector4 leftPalmRotation = Vector3.zero, rightPalmRotation = Vector3.zero;

            Frame frame = LeapService.CurrentFrame;
            foreach (Hand hand in frame.Hands)
            {
				if (hand.IsLeft)
				{
					leftPalmPosition = hand.PalmPosition.ToVector3 ();
					leftPalmRotation = new Vector4(hand.Rotation.x, hand.Rotation.y, hand.Rotation.z, hand.Rotation.w);
				}
				else
				{
					rightPalmPosition = hand.PalmPosition.ToVector3();
					rightPalmRotation = new Vector4(hand.Rotation.x, hand.Rotation.y, hand.Rotation.z, hand.Rotation.w);
				}
            }

			mStreamWriter.WriteLine(DataToString(leftPalmPosition, leftPalmRotation, rightPalmPosition, rightPalmRotation));
        }
	}

	private static string DataToString(Vector3 leftPalmPosition, Vector4 leftPalmRotation, Vector3 rightPalmPosition, Vector4 rightPalmRotation)
    {
		return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14}", Time.unscaledTime, 
			leftPalmPosition.x, leftPalmPosition.y, leftPalmPosition.z, 
			leftPalmRotation.x, leftPalmRotation.y, leftPalmRotation.z, leftPalmRotation.w, 
			rightPalmPosition.x, rightPalmPosition.y, rightPalmPosition.z,
			rightPalmRotation.x, rightPalmRotation.y, rightPalmRotation.z, rightPalmRotation.w);
	}

	public void UpdateGestureName(string name)
	{
		gestureName = name;
	}

	public void UpdateRecordingNumber(string number)
	{
		recordingNumber = System.Int32.Parse(number);
	}
}
