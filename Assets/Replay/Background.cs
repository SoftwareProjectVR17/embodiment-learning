﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
	bool display = true;

	public void ToggleDisplay()
	{
		if (!display)
		{
			this.gameObject.SetActive (true);
			display = true;
		}
		else
		{
			this.gameObject.SetActive (false);
			display = false;
		}
	}
}
