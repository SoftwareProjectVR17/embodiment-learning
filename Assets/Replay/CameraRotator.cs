﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{
	public GameObject CameraTarget;
	public float cameraTargetHeight = 0.0f;
	public int ZoomRate = 20;
	public float MaxViewDistance = 15f;
	public float MinViewDistance = 1f;

	private float x = 0.0f;
	private float y = 0.0f;
	private int mouseXSpeedMod = 5;
	private int mouseYSpeedMod = 5;
	private float distance = 3f;
	private float desireDistance;
	private float correctedDistance;
	private float currentDistance;

	private Vector3 initialPosition;
	private Quaternion initialRotation;
	private bool useInitialTransform = true;

	public void ToggleUseInitialTransform()
	{
		if (useInitialTransform)
			useInitialTransform = false;
		else
		{
			transform.position = initialPosition;
			transform.rotation = initialRotation;
			useInitialTransform = true;
		}
	}

	void Start ()
	{
		initialPosition = transform.position;
		initialRotation = transform.rotation;

		Vector3 Angles = transform.eulerAngles;
		x = Angles.x;
		y = Angles.y;
		currentDistance = distance;
		desireDistance = distance;
		correctedDistance = distance;
	}

	void LateUpdate ()
	{
		if (!useInitialTransform)
		{
			if (Input.GetMouseButton (0)) //left click
			{
				x += Input.GetAxis("Mouse X") * mouseXSpeedMod;
				y -= Input.GetAxis("Mouse Y") * mouseYSpeedMod;
			}

			//apply
			y = ClampAngle (y, -15, 25);
			Quaternion rotation = Quaternion.Euler (y,x,0);

			desireDistance -= Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * ZoomRate * Mathf.Abs(desireDistance);
			desireDistance = Mathf.Clamp (desireDistance, MinViewDistance, MaxViewDistance);
			correctedDistance = desireDistance;

			Vector3 position = CameraTarget.transform.position - (rotation * Vector3.forward * desireDistance);

			RaycastHit collisionHit;
			Vector3 cameraTargetPosition = new Vector3 (CameraTarget.transform.position.x, CameraTarget.transform.position.y + cameraTargetHeight, CameraTarget.transform.position.z);

			bool isCorrected = false;
			if (Physics.Linecast (cameraTargetPosition, position, out collisionHit))
			{
				position = collisionHit.point;
				correctedDistance = Vector3.Distance(cameraTargetPosition,position);
				isCorrected = true;
			}

			currentDistance = !isCorrected || correctedDistance > currentDistance ? Mathf.Lerp(currentDistance,correctedDistance,Time.deltaTime * ZoomRate) : correctedDistance;

			position = CameraTarget.transform.position - (rotation * Vector3.forward * currentDistance + new Vector3 (0, -cameraTargetHeight, 0));

			transform.rotation = rotation;
			transform.position = position;
		}
	}

	private static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle,min,max);
	}
}
