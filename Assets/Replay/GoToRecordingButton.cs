﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToRecordingButton : MonoBehaviour {

	public void GoToRecordingScene()
	{
		SceneManager.LoadScene ("Record");
	}
}
