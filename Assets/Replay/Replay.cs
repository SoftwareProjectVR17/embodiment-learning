﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Replay: MonoBehaviour
{
	public float rotationFactor = 2.0f;
	public GameObject handModel;
	public Dropdown dropdown;
	public Text textFrame;

	public Gesture gestureToReplay;
	public Gesture gestureVariance;
	public int currentFrame = 0;
	float lastTimestamp = 0;
	bool startReplay = false;
	List<string> replays = new List<string>();

	void Start()
	{
		LoadReplayList ();
	}

	void LoadReplayList()
	{
		dropdown.ClearOptions ();

		//todo: detect folders in Assets/Record/Recorded
		replays.Clear();
		string path = "Assets/Record/Recorded/";
		foreach (string replayPath in Directory.GetDirectories (path))
			replays.Add(replayPath.Remove(0, replayPath.LastIndexOf('/') + 1));

		dropdown.AddOptions (replays);

		UpdateSelectedGesture (0);
	}

	public void StartReplay()
	{
		lastTimestamp = Time.unscaledTime;
		startReplay = true;
		currentFrame = 0;
	}

	public void UpdateSelectedGesture(int id)
	{
		gestureToReplay = new Gesture (replays[id], Gesture.GestureType.Mean);
		gestureVariance = new Gesture (replays[id], Gesture.GestureType.Variance, gestureToReplay);
		currentFrame = 0;
		ApplyCurrentFrameToModel ();
	}

	void Update ()
	{
		if (startReplay)
		{
			if (currentFrame < gestureToReplay.states.Count - 1)
			{
				//if replay not finished
				//compute criteria to go to the next frame
				bool goToNextFrame = false;
				if (currentFrame == 0)
					goToNextFrame = true;
				else
				{
					float timePast = Time.unscaledTime - lastTimestamp;
					if (timePast >= gestureToReplay.states [currentFrame + 1].timestamp - gestureToReplay.states [currentFrame].timestamp)
						goToNextFrame = true;
				}

				//apply next frame
				if (goToNextFrame)
				{
					currentFrame++;
					ApplyCurrentFrameToModel ();
				}
			}
			else
				startReplay = false;
		}
	}

	void ApplyCurrentFrameToModel()
	{
		handModel.transform.position = gestureToReplay.states [currentFrame].position;

		float factor = 180 / 3.14f * rotationFactor;
		Vector4 rot = gestureToReplay.states [currentFrame].rotation * factor;

		handModel.transform.rotation = Quaternion.Euler (rot);


		lastTimestamp = Time.unscaledTime;

		//update UI
		textFrame.text = "Frame: " + currentFrame;
	}
}
